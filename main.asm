%define STR_LENGTH 256
%define ERR_EXIT 1

%include "lib.inc"
%include "dict.inc"
%include "words.inc"


section .rodata   ; make error strings read-only
    err_read_error:   db "Error: read error", 0
    err_too_long:     db "Error: string is too long", 0
    err_not_fnd:      db "Error: string is not found", 0

section .bss
    input: resb STR_LENGTH-1

section .text

global _start

_start:
    xor rax, rax
    xor rdi, rdi
    lea rsi, [input]
    mov rdx, STR_LENGTH
    syscall
    test rax, rax         ; test that rax >= 0
    mov rdi, err_read_error ; else read error
    jl .err
    cmp rax, STR_LENGTH  ; rax < STR_LENGTH
    mov rdi, err_too_long ; else input string is too long
    jge .err

    lea rdi, [input]
    mov rsi, first_word   ; start address of dictionary
    call find_word
    test rax, rax         ; test that rax not 0
    mov rdi, err_not_fnd  ; else input string is not found
    jz .err 

    mov rdi, rax          ; rdi is now a dictionary key address

    push rdi              ; store callee-saved 
    call string_length    ; calculate lenght of string
    pop rdi               ; restore callee-saved

    add rdi, rax          ; rdi is now a dictionary value
    inc rdi               ; skip null-terminated

    call print_string
    call print_newline

    xor rdi, rdi
    jmp exit

  .err:
    call error_string
    mov rdi, ERR_EXIT
    jmp exit


