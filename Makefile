ASM=nasm
ASMFLAGS=-f elf64 -g
LD=ld
PYC=python3

ASM_FILES=lib.asm dict.asm main.asm
OBJ_FILES=$(ASM_FILES:.asm=.o)
%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

words.inc: colon.inc
main.asm: words.inc

program: $(OBJ_FILES) 
	$(LD) -o $@ $^

.PHONY: clean
clean:
	rm -rf *.o program

.PHONY: test
test:
	make clean
	make program
	$(PYC) test.py


