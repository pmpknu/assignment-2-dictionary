%include "lib.inc"

section .text
global find_word

; It scans the entire dictionary for a matching key. 
; If it finds a match, it returns the starting address of the match.
; Otherwise, it returns 0. 
; @param rdi -- string address
; @param rsi -- list start pointer
find_word:
    push rsi      ; store callee-saved register
    mov rcx, rsi  ; store matching string address

    .loop:
        add rcx, 8          ; skipping value of pointer of previous element
        mov rsi, rcx        ; now here is an address of dictionary value
        push rcx
        call string_equals  ; compare with rdi string, result in rax
        pop rcx

        test rax, rax       ; 1 -- equals
        jnz .equals         ; 0 -- not equals

        sub rcx, 8          ; store value of pointer of previous element again
        mov rcx, [rcx]      ; switch to next element

        test rcx, rcx       ; pointer state end of dictionary
        jz .not_equals      ; end searching

        jmp .loop


    .equals:
        mov rax, rsi        ; return dictionary address of value 
        jmp .return
    .not_equals:
        xor rax, rax
    .return:
        pop rsi             ; restore callee-saved
        ret
