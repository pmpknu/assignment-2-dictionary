%define last 0; Storing a pointer to the last created item in the list

%macro colon 2
    %ifstr %1     ; Check existence of string
        %ifid %2  ; Check existence of id
            %2: 
                dq last         ; Pointer to the previos element
                db %1, 0        ; Storing null-terminated string under %2 label
                %define last %2 ; Store new old pointer
        %else
            %error "Invalid ID"
        %endif
    %else
        %error "Invalid input"
    %endif
%endmacro

