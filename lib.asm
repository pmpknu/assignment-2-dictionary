section .text

%define SYS_EXIT  60
%define SYS_WRITE 1
%define SYS_READ  0

%define STDOUT 1 
%define STDIN  0
%define STDERR 2

%define ASCII_SPACE     0x20
%define ASCII_TAB       0x9
%define ASCII_NEW_LINE  0xA

global exit
global string_length
global print_string
global print_newline
global print_char
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global error_string

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
  .loop:
    cmp   byte[rax+rdi], 0
    je    .end
    inc   rax
    jmp   .loop
  .end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rax, SYS_WRITE
    mov rdi, STDOUT
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
error_string:
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rax, SYS_WRITE
    mov rdi, STDERR 
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, ASCII_NEW_LINE

; Принимает код символа и выводит его в stdout
print_char:
    push  rdi
    mov   rax, SYS_WRITE  ; 'write' syscall number 
    mov   rdi, STDOUT     ; stdout descriptor
    mov   rsi, rsp        ; address
    mov   rdx, 1          ; lenght (one chat -- one byte)
    syscall
    pop   rdi
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbp
    mov rbp, rsp
    mov r10, 10
    mov rax, rdi
    mov r8, rsp
    sub rsp, 64
    dec r8
    mov byte[r8], 0
  .loop:
    xor rdx, rdx
    div r10 ; rax = неполное частное; rdx = остаток
    add dl, '0'
    dec r8
    mov [r8], dl
    test rax, rax
    jnz .loop
    mov rdi, r8
    call print_string
    mov r8, rbp
    add rsp, 64
    pop rbp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jge .greater
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    call print_uint    
    ret
  .greater:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
  .loop:
    mov al, byte[rdi+rcx]
    cmp al, byte[rsi+rcx]
    jne .not_equals
    test al, al
    jz  .equals
    inc rcx
    jmp .loop
  .equals:
    mov rax, 1
    ret
  .not_equals:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, SYS_READ   ; 'read' syscall number
    mov rdi, STDIN      ; stdin descriptor
    mov rdx, 1          ; will read 1 byte 
    dec rsp             ; allocate 1 byte
    mov rsi, rsp        ; where we locate
    syscall
    test rax, rax 
    jz .quit_read
    mov al, [rsp]
  .quit_read:
    inc rsp
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; IN  rdi -- beginning of buffer ; rsi -- buffer size
; OUT rax -- address of buffer (0 if failure)
;     rdx -- lenght of the word
read_word:
    push    rbx ; using callee-saved registers
    push    r12
    push    r13

    mov     rbx, rdi  ; beginning of buffer
    mov     r12, rsi  ; buffer size
    dec     r12       ; store 1 byte for null-terminate 
    xor     r13, r13  ; word lenght = 0
    
  .loop:
    cmp     r12, r13
    jge     .skip_whitespace
    jmp     .too_large

  .skip_whitespace:
    call    read_char
  
    cmp     al, 0
    je      .done

    cmp     al, ASCII_SPACE       ; space
    je      .skip_space

    cmp     al, ASCII_TAB         ; tab
    je      .skip_space

    cmp     al, ASCII_NEW_LINE    ; new line
    je      .skip_space

    mov     byte[rbx + r13], al   ; save to dest with word lenght shift
    inc     r13
    jmp     .loop

  .skip_space:
    test    r13, r13
    jnz     .done
    jmp     .loop

  .done:
    mov     byte[rbx + r13], 0    ; Null-terminate the string
    mov     rax, rbx
    mov     rdx, r13
    jmp     .word_end
  .too_large:
    xor     rax, rax              ; Set rax to 0    .return:
  .word_end:
    pop     r13                   ; restore callee-saved registers
    pop     r12
    pop     rbx
  
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; rdi - указатель на строку
parse_uint:
    push rbx      ; using callee-saved registers
    push r12      
    push r13
    mov r12, 10   ; system base 10
    xor r13, r13  ; answer lenght
    xor rbx, rbx  ; 
    xor rax, rax  ; answer
    xor rdx, rdx  ; clear the high 64 bits of the dest

  .loop:
    movzx rbx, byte[rdi + r13] ; read current symbol 

    test rbx, rbx              ; checking null-terminate
    jz .end

    cmp byte [rdi+r13], '0'    ; char code is below '0' 
    jb .end                    ;         -- not number

    cmp byte [rdi+r13], '9'    ; char code is above '9' 
    ja .end                    ;         -- not number

    sub rbx, '0'    ; getting number itself (not ASCII code)
    mul r12         
    add rax, rbx    
    inc r13         ; increment answer lenght
    jmp .loop

  .end:
    mov rdx, r13  ; set answer lenght
    pop r13       ; restore callee-saved registers
    pop r12
    pop rbx

    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp   byte[rdi], '-'
    je    .signed
    cmp   byte[rdi], '+'
    je    .unsigned
    jmp   parse_uint
  .unsigned:
    inc   rdi
    call  parse_uint
    inc   rdx
    ret
  .signed:
    inc   rdi         ; move pointer forward
    call  parse_uint
    neg   rax
    inc   rdx         ; increment string lenght
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi -- pointer to a string
; rsi -- pointer to a buffer
; rdx -- buffer length
string_copy:
    xor   rax, rax ; counter
  .loop:
    cmp   rax, rdx   ;  check fit into buffer
    jg    .overflow

    mov   cl, byte[rdi + rax] ; copy data
    mov   byte[rsi + rax], cl

    inc rax
    test cl, cl ; checking for null-terminate
    js .end
    jmp .loop

  .overflow:
    xor rax, rax
  .end:
    ret
